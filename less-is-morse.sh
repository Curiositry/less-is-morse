#!/bin/sh

if [ ! -d ~/less-is-morse ]; then
  sudo apt-get install git python python-pip
  /bin/echo Y
  sudo pip install cherrypy ws4py socketIO-client
  git clone https://gitlab.com/Curiositry/less-is-morse.git
fi

cd ~/less-is-morse
python morse_server.py
python morse_client.py