# -*- coding: utf-8 -*-

from ws4py.client.threadedclient import WebSocketClient
import RPi.GPIO as GPIO
import time
import datetime

# Setup the pins (we picked two BCM pins that have ground adjacent to them)
keyPin = 07
spkrPin = 10

# We're using BCM
GPIO.setmode(GPIO.BCM)

# Set a pullup on the keyPin, and set it as input
GPIO.setup(keyPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
# Speaker is output
GPIO.setup(spkrPin, GPIO.OUT)

# host = "localhost"
host = "127.0.0.1"
port = 8000

def microseconds ():
    keytime = datetime.datetime.now()
    keyseconds = keytime.second
    keyuseconds = keytime.microsecond
    return (keyseconds*1000000)+keyuseconds
    

class MorseClient(WebSocketClient):
    def opened(self):                
        #self.send("Hello, World!")
        print("Didn't send helolow")

    def closed(self, code, reason):
        print(("Closed down", code, reason))

    def received_message(self, m):
        # Print the message
        print(m)
        # Split the string into value + timestamp
        value = str(m)[0:1]
        if value == 0:
            anti_value = 1
        else:
            anti_value = 0
        dur = str(m)[1:]
        receiveTs = int(microseconds())
        print("value: "+value)
        print("time elapsed: "+str(microseconds() - receiveTs))
        print("duration: "+dur)
        while (microseconds() - receiveTs) < dur:
            print("start audio")
            GPIO.output(spkrPin, int(value))
        print("stop audio")
        GPIO.output(spkrPin, int(anti_value))

        
        # if value == "DN":
        #     # while (int(microseconds()) - int(key.lastTs)) >= (int(microseconds()) - int(receiveTs)):
        #     if GPIO.input(keyPin) == "LOW":
        #         GPIO.output(spkrPin, 1)
        #     key.lastTs = ts
        #     key.lastValue = "DN"
        #     # break
        #     
        # elif value == "UP":
        #     # while (int(microseconds()) - int(key.lastTs)) >= (int(microseconds()) - int(receiveTs)):
        #     if GPIO.input(keyPin) == "LOW":
        #         GPIO.output(spkrPin, 0)
        #     key.lastTs = ts
        #     key.lastValue = "UP"
        #     # break



class MorseKey():
    
    def keyEvent(self, channel):
        
        input = GPIO.input(keyPin)

        # Note: The timestamp bit still needs more work...
        
        if ((input == False) and (key.last_state != False)):
                key.last_end = microseconds()
                key.last_dur = key.last_end - key.last_start
                ws.send(str(key.last_state) + str(key.last_dur))
                key.last_state = 0
                key.last_start = microseconds()
                
            
        elif input and (key.last_state != True):
                key.last_end = microseconds()
                key.last_dur = key.last_end - key.last_start
                ws.send(str(key.last_state) + str(key.last_dur))
                key.last_state = 1
                key.last_start = microseconds()


    def listen(self, keyPin):
        GPIO.add_event_detect(keyPin, GPIO.BOTH, callback=self.keyEvent)

if __name__ == '__main__':
    
    try:
        ws = MorseClient('ws://'+host+':'+str(port)+'/ws', protocols=['http-only', 'morse'])
        ws.daemon = False
        ws.connect()
        #ws.send("Initialized")
    except KeyboardInterrupt:
        ws.close()

    key = MorseKey()
    key.last_state = 1
    key.last_dur = 0
    key.last_start = 0
    key.last_end = 0
    key.listen(keyPin)

# GPIO.cleanup()
